import socket
import sys
import time
import threading
from appJar import gui
import select
from socket import error as socket_error

#ustawienie globalnych zmiennych
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
global gdzie
gdzie = 0
global usr
usr = ""
global jakiChat
jakiChat = 0
global chatName
chatName = ""
global back2
back2 = 0

#funkcja do obslugi wszytkich buttonow (reakcja na klikniecie)
def press(button):
	global gdzie
	global usr
	global jakiChat
	global chatName
	global back2
	
	#buttony zamykajace okna polaczenia z serwerem oraz logowania
	if button == "Clos1" or button == "Clos2":
		gdzie = -1
		app.stop()
		
	#button Submit z IRC communicator, okna polaczenia z serwerem (podejmuje probe polaczenia sie z serwerem)
	elif button == "Sub1":
		if app.getEntry("IP/Domain Adress") == "":
			app.warningBox("Wrong adress", "You set wrong IP/Domain Adress!", parent = "IRC communicator") 
		elif app.getEntry("Port") == "":
			app.warningBox("Wrong port", "You set empty port adress", parent = "IRC communicator") 
		elif app.getEntry("Port").isdigit() <> True:
			app.warningBox("Wrong port", "You set wrong port adress, only numbers!", parent = "IRC communicator") 		
		else:
			global usr
			#server_address = ('192.168.1.104', 4615)	
			tmp = 0			
			try:
				hostname = app.getEntry("IP/Domain Adress")
				port = int(app.getEntry("Port"))
				
				#lokalizowanie serwera na podstawie podanej domeny/IP i portu
				server_details = socket.getaddrinfo(hostname, port)
				server_address = server_details[-1]
				server_address = server_address[-1]
				print(server_address)
				sock.connect(server_address)
				tmp = tmp + 1
			except Exception, e:
				print type(e)
				print e
			if tmp == 1:
				app.hideSubWindow("IRC communicator")
				app.showSubWindow("Settings")
			else:
				app.warningBox("Wrong address or Port", "You set wrong IP/Domain Address or Port!", parent = "IRC communicator")
				
	#button Submit Settings, okna logowania do serwera (wysyla komunikat z wpisanym loginem)
	elif button == "Sub2":
		if app.getEntry("Username") == "":
			app.warningBox("Wrong user name", "You set empty user name!", parent = "Settings")
		elif " " in app.getEntry("Username"):
			app.warningBox("Wrong user name", "You can't use spaces!", parent = "Settings")
		usr = app.getEntry("Username")
		
		#proba logowania, czekanie na odpowiedz serwera czy login jest unikatowy
		sock.send(usr)
		data = sock.recv(256)
		if data[:8] == "startyes":
			app.hideSubWindow("Settings")
			app.showSubWindow("Lobby")
			app.setLabel("l1", "Your name: " + usr)
			app.setLabel("l2","Your name: " + usr)
			t = threading.Thread(target=ThreadBehavior)
			t.start()
			gdzie = 1
		elif data[:8] == "startnoo":
			app.warningBox("Wrong user name", "Sorry, that username is already taken!", parent = "Settings")
	
	#button Select z Lobby, glownego okna komunikatora (dolaczenie do wybranego czatu grupowego lub wyslanie zaproszenia do rozmowy prywatnej)
	elif button == "Select":
		gdzie = 2
		app.enableTextArea("Messages")
		app.clearTextArea("Messages")
		app.disableTextArea("Messages")
		app.setEntry("Type here: ", "")
		app.showButton("Send")
		if app.getListBox("chatList"):	
			app.showLabel("l3")
			app.showListBox("groupUserList")
			chatName = str(app.getListBox("chatList"))
			chatName = chatName[2:-2]
			yolo = "jg" + chatName + "\0"
			sock.send(yolo)
			app.hideSubWindow("Lobby")
			app.showSubWindow("Chat")
			app.setLabel("name1", "Group Chat Name: " + chatName)
			jakiChat = 1
		elif app.getListBox("userList"):		
			app.hideLabel("l3")			
			chatName = str(app.getListBox("userList"))
			chatName = chatName[2:-2]
			app.updateListBox("groupUserList", [chatName])
			yolo = "in" + chatName + "\0"
			sock.send(yolo)
			app.setLabel("l4", "Invite send to " + chatName + "!")
			app.showSubWindow("Waiting")
			
	#button Send z Chat, okna czatu (wysyla wpisana wiadomosc do serwera, z odpowiednim prefix'em)
	elif button == "Send":
		if jakiChat == 1:
			message = app.getEntry("Type here: ").encode('utf-8')		
			yolo = "mg" + chatName + " " + message + "\0"
			sock.send(yolo)
			app.setEntry("Type here: ", "")
		elif jakiChat == 2:
			message = app.getEntry("Type here: ").encode('utf-8')
			yolo = "mu" + message + "\0"
			sock.send(yolo)
			app.setEntry("Type here: ", "")
			
	#button Cancel z Waiting, okna oczekiwania na akceptacje wyslanego zaproszenia (wysyla komunikat o anulowaniu zaproszenia)
	elif button == "Cancel":
		app.hideSubWindow("Waiting")
		sock.send("ca" + chatName + "\0")
		
	#button Accept z Invite, okna otrzymania zaproszenia do rozmowy prywatnej (wysyla komunikat o zaakceptowaniu zaproszenia)
	elif button == "Accept":
		app.hideSubWindow("Invite")
		app.hideSubWindow("Lobby")
		sock.send("ye"+chatName + "\0")
		app.updateListBox("groupUserList", [chatName])
		app.hideLabel("l3")
		app.showSubWindow("Chat")
		app.setLabel("name1", "User name: " + chatName)
		jakiChat = 2
		
	#button Dismiss z Invite, okna otrzymania zaproszenia do rozmowy prywatnej (wysyla komunikat o odrzuceniu zaproszenia)
	elif button == "Dismiss":
		app.hideSubWindow("Invite")
		sock.send("cb" + chatName + "\0")
		
	#button Back z Chat, okna czatu (wysyla komunikat o opuszczeniu odpowiedniego czatu - grupowego lub prywatnego)
	elif button == "Back":
		if jakiChat == 1:
			yolo = "lg" + chatName + "\0"
			sock.send("lg")
		if jakiChat == 2 and back2 == 0:
			sock.send("lu")
		if back2 == 1:
			sock.send("lw")
			back2 = 0
		app.hideSubWindow("Chat")
		app.showSubWindow("Lobby")
		app.enableTextArea("Messages")
		app.clearTextArea("Messages")
		app.disableTextArea("Messages")
		gdzie = 1
		jakiChat = 0
		
	#button Exit z Lobby, okna glownego komunikatora (powoduje wylaczenie aplikacji)
	elif button == "Exit":
		gdzie = -1
		app.stop()
		
	#button OK z okna Server not responding, okna bledu (powoduje wylaczenie aplikacji)
	elif button == "OK :(":
		gdzie = -1
		app.stop()
		
		
#obsluga watku od odbierania wiadomosci z serwera
def ThreadBehavior():
	global chatName
	global back2
	global jakiChat
	global gdzie
	
	#petla, dopoki uzytkownik nie wylaczyl aplikacji
	while gdzie is not -1:
		ready = select.select([sock], [], [], 0.4)
		if gdzie > 0 and ready[0]:
			data = ""
			data = sock.recv(256)
			if len(data) == 0:
				global crash
				app.showSubWindow("Server not responding")
				break
				
			#aktualizacja aktywnych uzytkownikow (po prawej stronie Lobby, glownego okna komunikatora)
			elif data[:6] == "active":
				data = data[6:]
				data = data[:data.index("\x00")]
				lista = data.split(" ")
				lista = lista[:-1]
				lista.remove(usr)
				app.updateListBox("userList", lista, select=False)
				
			#otrzymanie wiadomosci z czatu na ktorym aktualnie przebywa uzytkownik
			elif data[:2] == "mg":
				data = data[2:]
				data = data[:data.index('\x00')]
				app.enableTextArea("Messages")
				app.setTextArea("Messages",  data + "\n")
				app.disableTextArea("Messages")
				
			#aktualizacja listy uzytkownikow obecnych na czacie
			elif data[:8] == "groupRef":
				tmpUser = data[9:data.index(";")]
				if data[8] == 'J':
					app.enableTextArea("Messages")
					app.setTextArea("Messages",  "SYSTEM> " + tmpUser + " join the chat! :)" + "\n")
					app.disableTextArea("Messages")
				if data[8] == 'L':
					app.enableTextArea("Messages")
					app.setTextArea("Messages", "SYSTEM> " +  tmpUser + " left the chat! :(" + "\n")
					app.disableTextArea("Messages")
				data = data[data.index(";")+1:]
				data = data[:data.index('\x00')]
				lista = data.split(" ")
				lista = lista[:-1]
				lista.remove(usr)
				app.updateListBox("groupUserList", lista, select=False)
				
			#otrzymanie zaproszenia do rozmowy prywatnej od innego uzytkownika
			elif data[:6] == "invite":
				data = data[6:]
				data = data[:data.index('\x00')]
				chatName = data
				text = data + " invite you to the chat!"
				app.setLabel("inviteLabel", text)
				app.showSubWindow("Invite")
				
			#zaproszony uzytkownik jest zajety
			elif data[:4] == "busy":	
				app.setLabelFg("l4", "red")
				app.setLabel("l4", "User is talking with someone else. :(")
				app.hideLabel("l5")
				time.sleep(1.5)
				app.hideSubWindow("Waiting")
				app.showLabel("l5")
				app.setLabelFg("l4", "black")
				
			#inny uzytkownik anulowal zaproszenie do rozmowy prywatnej
			elif data[:6] == "cancel":
				app.setLabel("inviteLabel", "User reject his invitation.")
				app.setLabelFg("inviteLabel", "red")
				time.sleep(1.5)
				app.hideSubWindow("Invite")
				app.setLabelFg("inviteLabel", "black")
				
			#inny uzytkownik zaakceptowal zaproszenie do rozmowy prywatnej
			elif data[:6] == "accept":
				app.hideSubWindow("Waiting")
				app.hideSubWindow("Lobby")
				app.showSubWindow("Chat")
				app.setLabel("name1", "User name: " + chatName)
				jakiChat = 2
			
			#inny uzytkownik odrzucil zaproszenie do rozmowy prywatnej
			elif data[:6] == "reject":
				app.setLabelFg("l4", "red")
				app.setLabel("l4", "User reject your invitation. :(")
				app.hideLabel("l5")
				time.sleep(1.5)
				app.hideSubWindow("Waiting")
				app.showLabel("l5")
				app.setLabelFg("l4", "black")
				
			#inny uzytkownik opuscil rozmowe prywatna
			elif data[:8] == "userLeft":
				app.enableTextArea("Messages")
				app.setTextArea("Messages",  "SYSTEM> " + chatName + " left the chat! :(" + "\n")
				app.disableTextArea("Messages")
				app.hideButton("Send")
				back2 = 1
			else:				
				print("Komunikat ktorego nie znam: " + data)
	sock.close()	
	
	
if __name__ == "__main__":

	#stworzenie tablicy do watkow, odczytanie argumentow
	threads = []
	args = sys.argv

	crash = 0
	
	#ustawienie glownego okna GUI
	app = gui("bug")
	app.hide()
	app.setLocation(0,0)
	
	#ustawienie okna laczenia sie z serwerem
	app.startSubWindow("IRC communicator")
	app.setBg("orange")
	app.setGeometry("400x200")
	app.setFont(18)
	app.addLabelEntry("IP/Domain Adress",0)
	app.addLabelEntry("Port",1)
	app.addNamedButton("Submit","Sub1",  press,2)
	app.addNamedButton( "Close","Clos1",  press,2)
	app.setButtonSticky("Clos1","right")
	app.setButtonSticky("Sub1","left")
	app.setFocus("IP/Domain Adress")
	app.stopSubWindow()
	app.showSubWindow("IRC communicator")
	
	#ustawienie okna logowania sie do serwera
	app.startSubWindow("Settings")
	app.setBg("orange")
	app.setGeometry("400x200")
	app.setFont(18)
	app.addLabelEntry("Username")	
	app.addNamedButton( "Submit","Sub2", press,2)
	app.addNamedButton(  "Close","Clos2", press,2)
	app.setButtonSticky("Clos2","right")
	app.setButtonSticky("Sub2","left")
	app.setFocus("Username")
	app.stopSubWindow()
	
	#ustawienie okna glownego komunikatora, z wyswietleniem dostepnych czatow grupowych i aktywnych uzytkownikow
	app.startSubWindow("Lobby")
	app.setBg("orange")
	app.setGeometry("800x450")
	app.setFont(18)
	app.addLabel("l1","text", 0, 0, 2)
	app.addLabel("groupChat","Choose your theme group chat", 1, 0)
	app.addLabel("playerChat","Choose user you want to talk", 1, 1)
	app.addListBox("chatList", ["OffTopic", "Skiing", "Knowledge", "KiwiBoys","School", "SKProject", "Mandarines", "Beliebers", "Minecraft", "SaltyLOL"],2, 0)
	app.addListBox("userList", [],2, 1)
	app.addButton("Select", press, 3, 0)
	app.addButton("Exit", press, 3, 1)
	app.stopSubWindow()
	
	#ustawienie okna czatu
	app.startSubWindow("Chat", modal = True)
	app.setBg("orange")
	app.setGeometry("1000x600")
	app.setFont(18)	
	app.addLabel("name1", 0 ,0)
	app.addLabel("l2","text", 0, 1)
	app.addLabel("l3","Users in Chat", 0, 2)
	app.addScrolledTextArea("Messages", 1, 0, 2)
	app.setTextAreaHeight("Messages", 15)
	app.setTextAreaWidth("Messages", 40)
	app.disableTextArea("Messages")
	app.addListBox("groupUserList", [],1, 2)
	app.addLabelEntry("Type here: ", 2, 0)	
	app.addButton("Send", press, 2, 1)
	app.addButton("Back", press, 2, 2)	
	app.setFocus("Type here: ")
	app.stopSubWindow()
	
	#ustawienie okna otrzymania zaproszenia do rozmowy prywatnej
	app.startSubWindow("Invite", modal = True)
	app.setBg("orange")
	app.setGeometry("400x200")
	app.setFont(18)
	app.addLabel("inviteLabel", "start")
	app.addButtons( ["Accept", "Dismiss"], press)
	app.stopSubWindow()
	
	#ustawienie okna oczekiwania na akceptacje zaproszenia do rozmowy prywatnej
	app.startSubWindow("Waiting", modal = True)
	app.setBg("orange")
	app.setGeometry("400x200")
	app.setFont(18)
	app.addLabel("l4", "Invite send! Waiting for user anwser.")
	app.addLabel("l5", "Waiting for user anwser...")
	app.addButtons( ["Cancel"], press)
	app.stopSubWindow()
	
	#ustawienie okna bledu w przypadku braku komunikacji z serweren
	app.startSubWindow("Server not responding", modal = True)
	app.setBg("orange")
	app.setGeometry("400x200")
	app.setFont(18)	
	app.addLabel("l6", "Server is not responding.\nPress OK to leave.")
	app.setLabelFg("l6","red")
	app.addButton("OK :(", press)
	app.stopSubWindow()

	# start the GUI
	app.go()

	
	

	

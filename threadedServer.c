#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#define SERVER_PORT 4615
#define QUEUE_SIZE 5


int isAlive[100];
int socketList[100];
char usrList[100][256];
int groupList[10][100];
char nameGroupList[10][256];
int talkingList[100];
pthread_mutex_t groupListMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t socketListMutex = PTHREAD_MUTEX_INITIALIZER;

//struktura zawierająca dane, które zostaną przekazane do wątku
struct thread_data_t
{
    int clientID;
    char msg[256];
};


//funkcja opisującą zachowanie wątku - musi przyjmować argument typu (void *) i zwracać (void *)
void *ThreadBehavior(void *t_data)
{
    pthread_detach(pthread_self());
    struct thread_data_t *th_data = (struct thread_data_t*)t_data;
	int pomm = 1;
	//petla obslugujaca logowanie sie klienta do systemu
    while(1) {
		int bytes = -1;
		strcpy((*th_data).msg, "");
		
		//oczekiwanie na przeslanie danych logowania przez klienta
		while(bytes < 0) {
			bytes = read(socketList[(*th_data).clientID], (*th_data).msg, 256);
		}
		pthread_mutex_lock(&socketListMutex);
		
		//obsluga w przypadku gdy gniazdo klienta zostalo zamkniete
		if(bytes == 0){
			pthread_mutex_lock(&socketListMutex);
			socketList[(*th_data).clientID] = -1;
			isAlive[(*th_data).clientID] = -1;
			pthread_mutex_unlock(&socketListMutex);
			pomm = 0;
            break;
		}
		
		//sprawdzenie, czy nazwa klienta nie jest juz zajeta
		int isValid = 1;
		for(int i=0;i<100;i++) {
			if(isAlive[i] == 1 && strcmp(usrList[i], (*th_data).msg) == 0) {
				isValid = 0;
				break;
			}
		}
		pthread_mutex_unlock(&socketListMutex);
		for(int i=0;i<10;i++) {
			if(strcmp(nameGroupList[i], (*th_data).msg) == 0) {
				isValid = 0;
				break;
			}
		}
		if(isValid){
			char str[256] = "startyes";
			write(socketList[(*th_data).clientID], str, 256);
			isAlive[(*th_data).clientID] = 1;
			break;
		}
		else {
			char str[256] = "startnoo";
			write(socketList[(*th_data).clientID], str, 256);
		}
	}
	
	//wchodzi, jezeli logowanie powiodlo sie
    if(pomm) {
		printf("%s connected to the server with ID: %d\n", (*th_data).msg, (*th_data).clientID);

		strcpy(usrList[(*th_data).clientID], (*th_data).msg);

		//aktualizacja listy aktywnych uzytkownikow (rozeslanie listy do wszystkich aktywnych)
		char str[256] = "active";
		   for(int i=0;i<100;i++) {
				if(isAlive[i] == 1) {
					strcat(str, usrList[i]);
					strcat(str, " ");
				}
		   }
		   for(int i=0;i<100;i++) {
			  if(isAlive[i] == 1) {  	
				write(socketList[i], str, 256);
			  }
		   }
		
		//petla obslugujaca wszystkie pozostale komunikaty wysylane przez klienta
		//kazdy komunikat zawiera identyfikujacy go skrot na poczatku (*th_data).msg[]
		while(1) {
			int bytes = -1;
			strcpy((*th_data).msg, "");
			while(bytes < 0) {
				bytes = read(socketList[(*th_data).clientID], (*th_data).msg, 256);
			}
			
			//'m' - komunikat typu "tresc wiadomosci" 
			if((*th_data).msg[0] == 'm') {
				//wiadomosc dla czatu grupowego (nazwa czatu od msg[2] do nastepnej spacji)
				if((*th_data).msg[1] == 'g') {
					int i = 2, j= 0;
					char groupName[50], message[256];
					while((*th_data).msg[i] != ' ') {
						groupName[j] = (*th_data).msg[i];
						i++;
						j++;
					}
					groupName[j] = '\0';
					i++;
					j = 0;
					while((*th_data).msg[i] != '\x00') {
						message[j] = (*th_data).msg[i];
						i++;
						j++;
					}
					message[j] = '\0';

					char finalMessage[256] = ""; 
					strcat(finalMessage, "mg");
					strcat(finalMessage, usrList[(*th_data).clientID]);
					strcat(finalMessage, "> ");
					strcat(finalMessage, message);
					int bity = 2 + strlen(usrList[(*th_data).clientID]) + 2 + strlen(message)  + 1;

					pthread_mutex_lock(&groupListMutex);
					for(int i=0;i<10; i++) {
						if( strcmp(groupName, nameGroupList[i]) == 0) {
							for(int j=0;j<100;j++) {
								if(groupList[i][j] == 1) {
									write(socketList[j], finalMessage, bity);	
								}
							}
							break;
						}
					}
					pthread_mutex_unlock(&groupListMutex);
				}

				//wiadomosc prywatna dla konkretnego uzytkownika (id adresata odczytane z talkingList)
				if((*th_data).msg[1] == 'u') {
					int i = 2, j= 0;
					char message[256];
					while((*th_data).msg[i] != '\x00') {
						message[j] = (*th_data).msg[i];
						i++;
						j++;
					}
					message[j] = '\0';
					
					char finalMessage[256] = ""; 
					strcat(finalMessage, "mg");
					strcat(finalMessage, usrList[(*th_data).clientID]);
					strcat(finalMessage, "> ");
					strcat(finalMessage, message);
					int bity = 2 + strlen(usrList[(*th_data).clientID]) + 2 + strlen(message)  + 1;
					write(socketList[(*th_data).clientID], finalMessage, bity);
					write(socketList[talkingList[(*th_data).clientID]], finalMessage, bity);
				}
			}

			//komunikat o dolaczeniu do czatu grupowego (nazwa czatu od msg[2] do znaku konca '\0')
			if((*th_data).msg[0] == 'j') {
				if((*th_data).msg[1] == 'g') {
					int i = 2, k=0;
					char groupName[50]= "";
					while((*th_data).msg[i] != '\0') {
						groupName[k] = (*th_data).msg[i];
						i++;
						k++;
					}
					groupName[k] = '\0';

					pthread_mutex_lock(&groupListMutex);

					for(int i=0;i<10; i++) {
						if( strcmp(groupName, nameGroupList[i]) == 0) {
							groupList[i][(*th_data).clientID] = 1;
							break;			
						}
					}

					//dodawanie aktywnych uczestnikow do czatow grupowych
					char str[256] = "";
					strcpy(str, "groupRefJ");
					strcat(str, usrList[(*th_data).clientID]);
					strcat(str, ";");

					for(int i=0;i<10; i++) {
						if( strcmp(groupName, nameGroupList[i]) == 0) {
							for(int j=0;j<100;j++) {
								if(groupList[i][j] == 1) {
									strcat(str, usrList[j]);
									strcat(str, " ");
								}
							}
							break;
						}
					}

					//powiadomienie pozostalych czlonkow czatu grupowego o dolaczeniu nowego uzytkownika
					for(int i=0;i<10; i++) {
						if( strcmp(groupName, nameGroupList[i]) == 0) {
							for(int j=0;j<100;j++) {
								if(groupList[i][j] == 1) {
									write(socketList[j], str, 256);	
								}
							}
							break;
						}
					}

					pthread_mutex_unlock(&groupListMutex);
				}
			}

			//komunikat o opuszczeniu czatu grupowego
			if((*th_data).msg[0] == 'l') {
				if((*th_data).msg[1] == 'g') {

					//usuniecie uzytkownika z czatu grupowego
					pthread_mutex_lock(&groupListMutex);
					int grupa = -1;
					for(int i=0;i<10; i++) {
						if(groupList[i][(*th_data).clientID] == 1) {
							groupList[i][(*th_data).clientID] = -1;
							grupa = i;
							break;
						}
					}

					//powiadomienie uczestnikow czatu o opuszczeniu czatu przez uzytkownika
					char str[256] = "";
					strcpy(str, "groupRefL");
					strcat(str, usrList[(*th_data).clientID]);
					strcat(str, ";");
					for(int i=0;i<100;i++) {
						if(groupList[grupa][i] == 1) {

							strcat(str, usrList[i]);

							strcat(str, " ");
						}
					}
					for(int j=0;j<100;j++) {
						if(groupList[grupa][j] == 1) {
							write(socketList[j], str, 256);	
						}
					}

					pthread_mutex_unlock(&groupListMutex);
				}
			}

			//komunikat o wyslaniu zaproszenia do czatu prywatnego (nazwa zaproszonego uzytkownika od msg[2] do znaku konca '\0')
			if((*th_data).msg[0] == 'i') {
				if((*th_data).msg[1] == 'n') {
					int i = 2, k=0, tmpUser;
					char userName[50]= "";
					while((*th_data).msg[i] != '\0') {
						userName[k] = (*th_data).msg[i];
						i++;
						k++;
					}
					userName[k] = '\0';

					pthread_mutex_lock(&groupListMutex);
					//odszukanie id uzytkownika po nazwie
					for(int i=0;i<100; i++) {
						if( strcmp(userName, usrList[i]) == 0 && isAlive[i] == 1) {
							tmpUser = i;
							break;			
						}
					}

					int czyGrupa = 0;
					//sprawdzenie czy uzytkownik nie jest w czacie grupowym
					for(int i =0;i<10;i++){
						if(groupList[i][tmpUser] == 1){
							czyGrupa = 1;
							break;
						}

					}
					
					//zaproszenie uzytkownika gdy nie jest w innym czacie prywatnym lub grupowym,
					//inaczej zwrot informacji o niepowodzeniu
					if(talkingList[tmpUser] == -1 && czyGrupa == 0) {
						talkingList[tmpUser] = (*th_data).clientID;
						talkingList[(*th_data).clientID] = tmpUser;
						char str[256] = "";
						strcat(str, "invite");
						strcat(str, usrList[(*th_data).clientID]);
						write(socketList[tmpUser], str, 256);	
					}
					else{
						char str[256] = "";
						strcat(str, "busy");
						write(socketList[(*th_data).clientID], str, 256);		
					}
					pthread_mutex_unlock(&groupListMutex);
				}
			}

			//komunikat o odrzuceniu zaproszenia do rozmowy prywatnej
			if((*th_data).msg[0] == 'c') {
				if((*th_data).msg[1] == 'a') {
					int i = 2, k=0, tmpUser;
					char userName[50]= "";
					while((*th_data).msg[i] != '\0') {
						userName[k] = (*th_data).msg[i];
						i++;
						k++;
					}
					userName[k] = '\0';

					pthread_mutex_lock(&groupListMutex);

					for(int i=0;i<100; i++) {
						if( strcmp(userName, usrList[i]) == 0 && isAlive[i] == 1) {
							tmpUser = i;
							break;			
						}
					}
					talkingList[tmpUser] = -1;
					talkingList[(*th_data).clientID] = -1;

					write(socketList[tmpUser], "cancel", 256);

					pthread_mutex_unlock(&groupListMutex);
				}			
			}

			//komunikat o anulowaniu zaproszenia do rozmowy prywatnej
			if((*th_data).msg[0] == 'c') {
				if((*th_data).msg[1] == 'b') {
					int i = 2, k=0, tmpUser;
					char userName[50]= "";
					while((*th_data).msg[i] != '\0') {
						userName[k] = (*th_data).msg[i];
						i++;
						k++;
					}
					userName[k] = '\0';

					pthread_mutex_lock(&groupListMutex);

					for(int i=0;i<100; i++) {
						if( strcmp(userName, usrList[i]) == 0 && isAlive[i] == 1) {
							tmpUser = i;
							break;			
						}
					}
					talkingList[tmpUser] = -1;
					talkingList[(*th_data).clientID] = -1;

					write(socketList[tmpUser], "reject", 256);

					pthread_mutex_unlock(&groupListMutex);
				}			
			}

			//komunikat o zaakceptowaniu zaproszenia do rozmowy prywatnej
			if((*th_data).msg[0] == 'y') {
				if((*th_data).msg[1] == 'e') {
					int i = 2, k=0, tmpUser;
					char userName[50]= "";
					while((*th_data).msg[i] != '\0') {
						userName[k] = (*th_data).msg[i];
						i++;
						k++;
					}
					userName[k] = '\0';
					pthread_mutex_lock(&groupListMutex);

					for(int i=0;i<100; i++) {
						if( strcmp(userName, usrList[i]) == 0 && isAlive[i] == 1) {
							tmpUser = i;
							break;			
						}
					}
					write(socketList[tmpUser], "accept", 256);
					pthread_mutex_unlock(&groupListMutex);
				}
			}

			//komunikat o wyjsciu z rozmowy prywatnej (jako pierwszy)
			if((*th_data).msg[0] == 'l') {
				if((*th_data).msg[1] == 'u') {
					
					pthread_mutex_lock(&groupListMutex);

					int tmpUser = talkingList[(*th_data).clientID];
					talkingList[(*th_data).clientID] = -1;

					pthread_mutex_unlock(&groupListMutex);

					write(socketList[tmpUser], "userLeft", 256);
				}	
			}

			//komunikat o wyjsciu z rozmowy prywatnej (gdy uzytkownik byl sam)
			if((*th_data).msg[0] == 'l') {
				if((*th_data).msg[1] == 'w') {

					pthread_mutex_lock(&groupListMutex);
					talkingList[(*th_data).clientID] = -1;

					pthread_mutex_unlock(&groupListMutex);
				}	
			}

			if(bytes == 0) {
				socketList[(*th_data).clientID] = -1;
				isAlive[(*th_data).clientID] = -1;
				break;
			}
		}
		printf("%s disconnected from the server\n", usrList[(*th_data).clientID]);

		//aktualizacja listy aktywnych uzytkownikow (rozeslanie listy do wszystkich aktywnych)
		strcpy(str, "active");
	    for(int i=0;i<100;i++) {
			if(isAlive[i] ==1) {
				strcat(str, usrList[i]);
				strcat(str, " ");
			}
	    }
 	    for(int i=0;i<100;i++) {
		  if(isAlive[i] == 1) {
			write(socketList[i], str, 256);
		  }
	    }
	}

    pthread_exit(NULL);
}


//funkcja obsługująca połączenie z nowym klientem
void handleConnection(int connection_socket_descriptor, int clientID) {
    //wynik funkcji tworzącej wątek
    int create_result = 0;

    //uchwyt na wątek
    pthread_t thread1;
    
    //dane, które zostaną przekazane do wątku
    struct thread_data_t* t_data = malloc(sizeof(struct thread_data_t));

    (*t_data).clientID = clientID;
	
	//stworzenie watku dla klienta
    create_result = pthread_create(&thread1, NULL, ThreadBehavior, (void *)t_data);
    if (create_result){
       printf("Błąd przy próbie utworzenia wątku, kod błędu: %d\n", create_result);
       exit(-1);
    }
}


int main(int argc, char* argv[])
{
	//ustawienie -1 w wszystkich tablicach, oznaczajace wolne miejsca
	pthread_mutex_lock(&socketListMutex);
    for(int i =0;i<100;i++){
        socketList[i] = -1;  
		talkingList[i] = -1;
		isAlive[i] = -1;
		for(int j=0;j<10;j++) {
			groupList[j][i] = -1;	
		}
    }
	pthread_mutex_unlock(&socketListMutex);
	
	//dodanie nazw poszczegolnych grup czatowych oraz 
	//stworzenie zmiennych do inicjalizacji gniazda serwera
	strcpy(nameGroupList[0], "OffTopic");
	strcpy(nameGroupList[1], "Skiing");
	strcpy(nameGroupList[2], "Knowledge");
	strcpy(nameGroupList[3], "KiwiBoys");
	strcpy(nameGroupList[4], "School");
	strcpy(nameGroupList[5], "SKProject");
	strcpy(nameGroupList[6], "Mandarines");
	strcpy(nameGroupList[7], "Beliebers");
	strcpy(nameGroupList[8], "Minecraft");
	strcpy(nameGroupList[9], "SaltyLOL");
   int server_socket_descriptor;
   int connection_socket_descriptor;
   int bind_result;
   int listen_result;
   char reuse_addr_val = 1;
   int clientID;
   
   struct sockaddr_in server_address;

   //inicjalizacja gniazda serwera
   
   memset(&server_address, 0, sizeof(struct sockaddr));
   server_address.sin_family = AF_INET;
   server_address.sin_addr.s_addr = htonl(INADDR_ANY);
   server_address.sin_port = htons(SERVER_PORT);

   server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
   if (server_socket_descriptor < 0) {
       fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda..\n", argv[0]);
       exit(1);
   }
   setsockopt(server_socket_descriptor, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_addr_val, sizeof(reuse_addr_val));

   bind_result = bind(server_socket_descriptor, (struct sockaddr*)&server_address, sizeof(struct sockaddr));
   if (bind_result < 0) {
       fprintf(stderr, "%s: Błąd przy próbie dowiązania adresu IP i numeru portu do gniazda.\n", argv[0]);
       exit(1);
   }

   listen_result = listen(server_socket_descriptor, QUEUE_SIZE);
   if (listen_result < 0) {
       fprintf(stderr, "%s: Błąd przy próbie ustawienia wielkości kolejki.\n", argv[0]);
       exit(1);
   }
	
   //petla obslugujaca prosby o dolaczenie do serwera
   while(1)
   {
       connection_socket_descriptor = accept(server_socket_descriptor, NULL, NULL);
       if (connection_socket_descriptor < 0)
       {
           fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda dla połączenia.\n", argv[0]);
           exit(1);
       }
	   
	   // dodanie deskryptora gniazda klienta do tablicy
       pthread_mutex_lock(&socketListMutex);
       for(int i=0;i<100;i++) {
           if(socketList[i] == -1) {
               clientID = i;
               break;
           } 
       }
       socketList[clientID] = connection_socket_descriptor;
	   isAlive[clientID] = 0;
	   pthread_mutex_unlock(&socketListMutex);
	   
	   //funkcja tworzaca watek dla klienta
       handleConnection(connection_socket_descriptor, clientID);
   }

   close(server_socket_descriptor);
   return(0);
}
